module gitlab.com/ptflp/gopubsub

go 1.19

require (
	github.com/confluentinc/confluent-kafka-go v1.9.2
	github.com/streadway/amqp v1.1.0
)
