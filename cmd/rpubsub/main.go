package main

import (
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/ptflp/gopubsub/rabbitmq"
	"log"
	"time"
)

// Пример использования
func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	rmq, err := rabbitmq.NewRabbitMQ(conn)
	if err != nil {
		panic(err)
	}
	defer rmq.Close()

	if err := rabbitmq.CreateExchange(conn, "test", "direct"); err != nil {
		log.Fatal(err)
	}

	messages, err := rmq.Subscribe("test")
	if err != nil {
		panic(err)
	}

	ticker := time.NewTicker(600 * time.Second)
	defer ticker.Stop()
	n := 1
	for {
		select {
		case <-ticker.C:
			break
		case msg := <-messages:
			err := rmq.Ack(&msg)
			if err != nil {
				panic(err)
			}
			fmt.Println(string(msg.Data))
		default:
			time.Sleep(100 * time.Millisecond)
			err = rmq.Publish("test", []byte(fmt.Sprintf("message Rabbitmq %d", n)))
			if err != nil {
				panic(err)
			}
			n++
		}
	}
	rmq.Close()
}
