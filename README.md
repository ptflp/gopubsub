# Gopubsub


## Запуск проекта

```bash
docker-compose up -d
```

## Запуск приложений

Pubsub RabbitMQ

Dashboard `http://localhost:15672/`

```bash
go run cmd/rpubsub/main.go
```


Pubsub Kafka

```bash
go run cmd/kpubsub/main.go
```